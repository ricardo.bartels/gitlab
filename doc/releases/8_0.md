---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#designated-technical-writers
---

# GitLab Cloud Native Chart 8.0

Along with the `17.0` release of GitLab, we have bumped the chart version to `8.0`.

## Summary of major changes

- Support for PostgreSQL 13 has been removed. Make sure you are running PostgreSQL 14 or newer before upgrading.

## Upgrade path from 7.x

To upgrade to the `8.0` version of the chart, you first need to upgrade to the latest `7.11.x`
release of the chart. Check the [version mapping details](../installation/version_mappings.md) for the latest patch.
